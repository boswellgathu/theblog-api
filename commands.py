#! /usr/bin/env python3
import click
import time
from plumbum import local, BG, FG, RETCODE


doco = local["docker-compose"]
doco_python = doco["run", "api"]
psql = local["psql"]
# postgresql://[user[:password]@][netloc][:port][/dbname][?param1=value1&...]
psql_connection = "postgresql://postgres:bloger@database"
pg_isready = local["pg_isready"]


@click.group()
def cli():
    pass


def wait_for_db():
    doco["up", "-d", "database"] & BG
    while pg_isready["-h", "database"] & RETCODE != 0:
        print("Waiting for Postgres database...")
        time.sleep(1)


@cli.command()
@click.option("--drop", "-d", is_flag=True)
def init_db(drop):
    wait_for_db()
    if drop:
        psql[psql_connection, "-c", "DROP DATABASE IF EXISTS theblog_dev"] & FG
    psql[psql_connection, "-f", "theblog/scripts/init.sql"] & FG
    doco_python["python", "scripts/create_db.py"] & FG


@cli.command()
def drop_db():
    psql[psql_connection, "-c", "DROP DATABASE IF EXISTS theblog_dev"] & FG


@cli.command(context_settings={"ignore_unknown_options": True})
@click.option("--watch", "-w", is_flag=True)
@click.argument("args", nargs=-1)
def test_api(watch, args):
    command = (
        ["pytest-watch"]
        if watch
        else ["python", "-m", "pytest", "--import-mode=append"]
    )

    doco["run", "api"][command] & FG


if __name__ == "__main__":
    cli()
