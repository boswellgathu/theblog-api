from sqlalchemy import Column, String, Integer
from db import Base


class User(Base):
    __tablename__ = "users"

    id = Column(Integer, primary_key=True)
    handle = Column(
        String(36), primary_key=True, index=True, unique=True, nullable=False
    )
    first_name = Column(String(1024))
    last_name = Column(String(1024))
    email = Column(String(256))
