#! /usr/bin/env python3
import os

from alembic import command
from alembic.config import Config

from db import Base, engine
from models import *


def create_db():
    """Create tables for all models, and set the latest alembic revision"""
    try:
        Base.metadata.create_all(engine)
        alembic_ini = os.path.join(
            os.path.abspath(os.path.dirname(__file__) + "/../"), "alembic.ini"
        )
        alembic_cfg = Config(alembic_ini)
        command.stamp(alembic_cfg, "head")
    except Exception as err:
        print('\n\n\n')
        print('Error 1')
        print(err)
        print('\n\n\n')

        raise


if __name__ == "__main__":
    create_db()
