-- Create database if it does not exist
SELECT
    'CREATE DATABASE theblog_dev'
WHERE
    NOT EXISTS (
        SELECT
        FROM
            pg_database
        WHERE
            datname = 'theblog_dev') \gexec

DO $body$
BEGIN
    -- Create user if it does not exist
    IF NOT EXISTS (
        SELECT
            *
        FROM
            pg_catalog.pg_user
        WHERE
            usename = 'bloger') THEN
    CREATE USER bloger WITH INHERIT LOGIN PASSWORD 'bloger';
END IF;
    -- Create migrations user for schema migrations if it does not exist
    IF NOT EXISTS (
        SELECT
            *
        FROM
            pg_catalog.pg_user
        WHERE
            usename = 'migrations') THEN
    CREATE USER migrations WITH INHERIT LOGIN PASSWORD 'migrations';
END IF;
END
$body$;

-- SCHEMAS = the collection of all schemas
-- PUBLIC = The PUBLIC Role
-- Allow SELECTs for everyone

ALTER DEFAULT PRIVILEGES GRANT USAGE ON SCHEMAS TO PUBLIC;

GRANT USAGE ON SCHEMA public TO PUBLIC;

ALTER DEFAULT PRIVILEGES FOR ROLE migrations GRANT
SELECT
    ON TABLES TO PUBLIC;

-- Give migrations full access to the public schema
GRANT ALL ON SCHEMA public TO migrations;

-- Protect against app being able to create / drop tables
REVOKE CREATE ON SCHEMA public FROM bloger;

-- Give bloger CRUD access to tables migrations creates
ALTER DEFAULT PRIVILEGES FOR ROLE migrations GRANT ALL ON TYPES TO bloger;

ALTER DEFAULT PRIVILEGES FOR ROLE migrations GRANT ALL ON SEQUENCES TO bloger;

ALTER DEFAULT PRIVILEGES FOR ROLE migrations GRANT INSERT, UPDATE, DELETE, REFERENCES ON TABLES TO bloger;

