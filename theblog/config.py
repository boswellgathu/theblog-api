from pathlib import Path

from pydantic import BaseSettings, Field
from typing import Dict, Any


class Settings(BaseSettings):
    db_port: int = Field(..., env="DATABASE_PORT", description="Database host port")
    db_user: str = Field("bloger", env="DATABASE_USER", description="Database user")
    db_pwd: str = Field("bloger", env="DATABASE_PASSWORD")
    db_host: str = Field("database", env="DATABASE_HOST")
    db_name: str = Field("theblog_dev", env="DATABASE_NAME")

    class Config:
        env_file = Path(".") / ".env"
        env_file_encoding = "utf-8"


config = Settings()

if __name__ == "__main__":
    print(config)
