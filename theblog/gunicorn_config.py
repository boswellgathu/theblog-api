import os
import multiprocessing

# "standard" recommendation for workers
workers = os.getenv("WORKERS", multiprocessing.cpu_count() * 2 + 1)
loglevel = os.getenv("LOG_LEVEL", "warning")
host = os.getenv("HOST", "0.0.0.0")
port = os.getenv("PORT", "3000")
bind = f"{host}:{port}"
# errorlog = os.getenv("LOG_DIR")
keepalive = os.getenv("KEEPALIVE", 24 * 60 * 60)  # 1 day
timeout = os.getenv("TIMEOUT", 60)  # 1 minute
reload = bool(os.getenv("RELOAD", False))

# must use uvicorn worker with async asgi framework (fastapi)
worker_class = "uvicorn.workers.UvicornWorker"
