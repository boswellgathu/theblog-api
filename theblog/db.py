from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

from config import config

engine = create_engine(
    f"postgresql://{config.db_user}:{config.db_pwd}@{config.db_host}:5432/{config.db_name}",
    echo=True
)
Session = sessionmaker(bind=engine)
Base = declarative_base()
