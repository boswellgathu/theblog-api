from config import Settings
import os


def test_converts_env_variable_to_type(monkeypatch):
    monkeypatch.setenv("DATABASE_PORT", "5433")
    actual = Settings()
    assert actual.db_port == 5433