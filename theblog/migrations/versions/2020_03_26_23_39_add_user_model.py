"""add user model

Revision ID: 364b3431b883
Revises: 
Create Date: 2020-03-26 23:39:17.828789

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "364b3431b883"
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        "users",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("handle", sa.String(length=36), nullable=False),
        sa.Column("first_name", sa.String(length=1024), nullable=True),
        sa.Column("last_name", sa.String(length=1024), nullable=True),
        sa.Column("email", sa.String(length=256), nullable=True),
        sa.PrimaryKeyConstraint("id", "handle"),
    )
    op.create_index(op.f("ix_users_handle"), "users", ["handle"], unique=True)


def downgrade():
    op.drop_index(op.f("ix_users_handle"), table_name="users")
    op.drop_table("users")
